import React from 'react';
import TopBar from './TopBar';
import Chan from './Chan'

const Main = () => {
    return (
        <div>
            <TopBar />
            <Chan />
        </div>
    );
}

export default Main;